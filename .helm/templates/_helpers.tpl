{{/*
Expand the name of the chart.
*/}}
{{- define "app.name" -}}
{{- default (printf "%s" .Release.Name ) .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s" .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "app.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "env" }}
{{- $root := index . 0 }}
{{- $envs := index . 1 }}
{{- $hook := index . 2 }}
{{- range $name, $item := $envs }}
- name: {{ $name }}
{{- if $item.value }}
  value: {{ $item.value | quote }}
{{- else if $item.external_secret }}
  valueFrom:
    secretKeyRef:
      name: {{ $item.external_secret.name | quote }}
      key: {{ $item.external_secret.key | quote }}
{{- else if $item.secret }}
  valueFrom:
    secretKeyRef:
    {{- if $hook }}
      name: {{ include "app.fullname" $root }}-hook
    {{- else }}
      name: {{ include "app.fullname" $root }}
    {{- end }}
      key: {{ $item.secret.key | quote }}
{{- end }}
{{- end }}
{{- end }}
